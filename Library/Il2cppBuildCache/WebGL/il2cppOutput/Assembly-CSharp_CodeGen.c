﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void AIControls::Start()
extern void AIControls_Start_m43DF58ECA5DB40047A478C519C2F12A16E5DF5DE (void);
// 0x00000002 System.Void AIControls::FixedUpdate()
extern void AIControls_FixedUpdate_m3644875AF7AB71CB67D1CA90AC50E6A59E9866F5 (void);
// 0x00000003 System.Void AIControls::Forward(System.Single)
extern void AIControls_Forward_m23B786BD0464A79546F1DF8DE975FFE348C487E6 (void);
// 0x00000004 System.Void AIControls::Move(System.Single)
extern void AIControls_Move_mF9A0993BF0B1DF977438A9153E5340E6E3FC19D6 (void);
// 0x00000005 System.Void AIControls::Turning(System.Single)
extern void AIControls_Turning_m023FB9796DC48CD80A913E4FF5E5984E59150544 (void);
// 0x00000006 System.Void AIControls::OnTriggerEnter(UnityEngine.Collider)
extern void AIControls_OnTriggerEnter_mE24544FAE4E6A3283068968516BDD3B95E724877 (void);
// 0x00000007 System.Void AIControls::OnTriggerExit(UnityEngine.Collider)
extern void AIControls_OnTriggerExit_m7C98CF2C7D8D8170CA6E4172D21EE0B3E8CC00B5 (void);
// 0x00000008 System.Void AIControls::OnTCollisionEnter(UnityEngine.Collision)
extern void AIControls_OnTCollisionEnter_mB0173C569FFE678D7866C5CEC2726F292D1F4613 (void);
// 0x00000009 System.Void AIControls::.ctor()
extern void AIControls__ctor_m38E3F626D212C15FA3079208CCB80706A65A1040 (void);
// 0x0000000A System.Void CreateAndDestroy::Start()
extern void CreateAndDestroy_Start_m1195C377BC92485AF426AF905B9D7DA1477C9704 (void);
// 0x0000000B System.Void CreateAndDestroy::Update()
extern void CreateAndDestroy_Update_m402FA29B26077C1F353EAA2B0BC714FFD91BFAAD (void);
// 0x0000000C System.Void CreateAndDestroy::.ctor()
extern void CreateAndDestroy__ctor_m7C5F81C9215D9CBAE55839BCC007A06F172DDEC4 (void);
// 0x0000000D System.Void CreateEnvironment::Start()
extern void CreateEnvironment_Start_m131A7D291F7F51FD2A8DB3606ADF9C498F7E2F2B (void);
// 0x0000000E System.Void CreateEnvironment::.ctor()
extern void CreateEnvironment__ctor_mCA994A52507CB95072D88B85AFB27F095166DB70 (void);
// 0x0000000F System.Void Follow::Update()
extern void Follow_Update_m92DDE81C25F59A864235D4E114204C3818D256EC (void);
// 0x00000010 System.Void Follow::.ctor()
extern void Follow__ctor_m2F6933C1E6E175D98CD967B20317B7EEF22ED34F (void);
// 0x00000011 System.Void GameController::Awake()
extern void GameController_Awake_mE678A4EEF012294E485A814D37D93231A90B4BEC (void);
// 0x00000012 System.Void GameController::Start()
extern void GameController_Start_mCC3F0292799528323E2217A12DB08D98CDD492AE (void);
// 0x00000013 System.Void GameController::Update()
extern void GameController_Update_mA76A2CE1F2AC1AACCDBF913CA6E1EA73DC621CD0 (void);
// 0x00000014 System.Void GameController::Restart()
extern void GameController_Restart_m4AEFD85E727195AC4517C6FBE488E42A584527B4 (void);
// 0x00000015 System.Void GameController::ObstacleDestroyed()
extern void GameController_ObstacleDestroyed_m7EE7A0F707069AA91AA878A33ABC29755A9C2C80 (void);
// 0x00000016 System.Void GameController::EnemyDestroyed()
extern void GameController_EnemyDestroyed_mCE0CFA77C3C7496336FE567A3C31AE243A1A108A (void);
// 0x00000017 System.Void GameController::SetHealth(System.Int32,System.Int32)
extern void GameController_SetHealth_m20DF11269095BB75C8490556524FBDDF430592AC (void);
// 0x00000018 System.Void GameController::.ctor()
extern void GameController__ctor_m9D952052C0A7234373FA5531292FCA8855BE2643 (void);
// 0x00000019 System.Void Health::Start()
extern void Health_Start_m67B788A33501A2CF22C3231FD3FCCD6D1D63A77F (void);
// 0x0000001A System.Void Health::ReduceHealth(System.Int32)
extern void Health_ReduceHealth_m51EB454D0BB7FE64EF560988B4CA4D7F43627E3C (void);
// 0x0000001B System.Collections.IEnumerator Health::DamageFlash()
extern void Health_DamageFlash_mBDB7434500C1E33F30DBF2E88AF14613C4BC46A9 (void);
// 0x0000001C System.Void Health::.ctor()
extern void Health__ctor_mD50C73D87211EAE260B38B2936F01722E31B9416 (void);
// 0x0000001D System.Void Health/<DamageFlash>d__11::.ctor(System.Int32)
extern void U3CDamageFlashU3Ed__11__ctor_m4C986E6DF43C7E1FAE7BDD2D7508782D3A55E581 (void);
// 0x0000001E System.Void Health/<DamageFlash>d__11::System.IDisposable.Dispose()
extern void U3CDamageFlashU3Ed__11_System_IDisposable_Dispose_m100A2EB7DE727C8034A8A193C67BE6CECCC82FD4 (void);
// 0x0000001F System.Boolean Health/<DamageFlash>d__11::MoveNext()
extern void U3CDamageFlashU3Ed__11_MoveNext_mD3964081C99FD6E50572757B0F63C086B5EDE6CE (void);
// 0x00000020 System.Object Health/<DamageFlash>d__11::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CDamageFlashU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA22FD54A285B343C4CD828D0394A4B8AB1F00707 (void);
// 0x00000021 System.Void Health/<DamageFlash>d__11::System.Collections.IEnumerator.Reset()
extern void U3CDamageFlashU3Ed__11_System_Collections_IEnumerator_Reset_mA54E160D0E0D3173D6FB9F0DA417A70F9D3D1762 (void);
// 0x00000022 System.Object Health/<DamageFlash>d__11::System.Collections.IEnumerator.get_Current()
extern void U3CDamageFlashU3Ed__11_System_Collections_IEnumerator_get_Current_m31DCB29C60C0159D6209268A26715276E8380A52 (void);
// 0x00000023 System.Void MainMenu::StartGame()
extern void MainMenu_StartGame_m8EFE49FE95784A266A683D0CCAAE7C2274981049 (void);
// 0x00000024 System.Void MainMenu::QuitGame()
extern void MainMenu_QuitGame_m9F32E266C6F6CE345067D062258362159D267030 (void);
// 0x00000025 System.Void MainMenu::.ctor()
extern void MainMenu__ctor_m4D77CEC8F91682A2D9492AE815F89B178BF9717D (void);
// 0x00000026 System.Void PlayerControls::Start()
extern void PlayerControls_Start_mB9489C35C70BB20AA18FC5359CFD9479116004D4 (void);
// 0x00000027 System.Void PlayerControls::Update()
extern void PlayerControls_Update_mA38B48E800B339AE629F0CA32238CD57E7271E84 (void);
// 0x00000028 System.Void PlayerControls::FixedUpdate()
extern void PlayerControls_FixedUpdate_mCDB2EC1A56136E6F64211730920AAF2648B575B8 (void);
// 0x00000029 System.Void PlayerControls::.ctor()
extern void PlayerControls__ctor_m892AB10F99012BE66F2524671783FDC93276FB8C (void);
// 0x0000002A System.Void Projectile::Start()
extern void Projectile_Start_m065C53350564E17D5A0A0322FF064F8C9697DAB6 (void);
// 0x0000002B System.Void Projectile::Update()
extern void Projectile_Update_m382C5B499BD4599FE34A04DA3DA0701077C710B2 (void);
// 0x0000002C System.Void Projectile::Explode()
extern void Projectile_Explode_mE3CE07A76C08E543158AC5340235061A2952A37B (void);
// 0x0000002D System.Void Projectile::OnTriggerEnter(UnityEngine.Collider)
extern void Projectile_OnTriggerEnter_m4692C1F9E4F8BF76298E88BE8657979932FFA02A (void);
// 0x0000002E System.Void Projectile::.ctor()
extern void Projectile__ctor_m22DAC83BA9B394316027755FD2ADFCA806EE39BB (void);
// 0x0000002F UnityEngine.GameObject Spawner::SpawnPlayer()
extern void Spawner_SpawnPlayer_mFB56841E48FE1184F00C65D283559AE3F12B8917 (void);
// 0x00000030 UnityEngine.GameObject Spawner::SpawnEnemy()
extern void Spawner_SpawnEnemy_mF1C790E6915E80A69115EABA6208DCB6EBE21CA3 (void);
// 0x00000031 UnityEngine.GameObject Spawner::SpawnEnemyRed()
extern void Spawner_SpawnEnemyRed_mDD9DD117EC7955F619DC4039A4C542115C29DC14 (void);
// 0x00000032 UnityEngine.GameObject Spawner::Spawn(UnityEngine.GameObject)
extern void Spawner_Spawn_mF3E97E2A9E5BAAE0B1F9032EB8116B48182B500A (void);
// 0x00000033 System.Void Spawner::.ctor()
extern void Spawner__ctor_m08E8D40AAA40F4329D8A95EEE2B2B6BE842CEB9C (void);
// 0x00000034 System.Void UIController::Update()
extern void UIController_Update_m01A435E125093AE9631843DBDD904FEF5C82D282 (void);
// 0x00000035 System.Void UIController::TogglePause()
extern void UIController_TogglePause_m28356A3A207477C5BF1DDCE081B83FE8863CA865 (void);
// 0x00000036 System.Void UIController::ShowRespawn(System.Boolean)
extern void UIController_ShowRespawn_m5C2B2E5BEAC57642454E45278900E2EBB327DD70 (void);
// 0x00000037 System.Void UIController::ShowEndScreen(System.Int32)
extern void UIController_ShowEndScreen_m4B94B8E8213F807A8A67EC40ED9C9AFFCBA62BBF (void);
// 0x00000038 System.Void UIController::QuitGame()
extern void UIController_QuitGame_m14442F0E80F0D3E4BBF0861C77CAFB32B099AB4A (void);
// 0x00000039 System.Void UIController::SetScore(System.Single)
extern void UIController_SetScore_m4256DEB3D595BD099A9A3BBEF63C8BC510DFA692 (void);
// 0x0000003A System.Void UIController::SetHealth(System.Int32,System.Int32)
extern void UIController_SetHealth_m499D2973518BC092533210E9F8C6EF580A8CAF48 (void);
// 0x0000003B System.Void UIController::SetLives(System.Int32,System.Int32)
extern void UIController_SetLives_m66A1F47834401B3A2C94FFE668DE317696E04DDA (void);
// 0x0000003C System.Void UIController::.ctor()
extern void UIController__ctor_m57DAF9FADDD58C79489BCF6474E55D3514E9BA21 (void);
static Il2CppMethodPointer s_methodPointers[60] = 
{
	AIControls_Start_m43DF58ECA5DB40047A478C519C2F12A16E5DF5DE,
	AIControls_FixedUpdate_m3644875AF7AB71CB67D1CA90AC50E6A59E9866F5,
	AIControls_Forward_m23B786BD0464A79546F1DF8DE975FFE348C487E6,
	AIControls_Move_mF9A0993BF0B1DF977438A9153E5340E6E3FC19D6,
	AIControls_Turning_m023FB9796DC48CD80A913E4FF5E5984E59150544,
	AIControls_OnTriggerEnter_mE24544FAE4E6A3283068968516BDD3B95E724877,
	AIControls_OnTriggerExit_m7C98CF2C7D8D8170CA6E4172D21EE0B3E8CC00B5,
	AIControls_OnTCollisionEnter_mB0173C569FFE678D7866C5CEC2726F292D1F4613,
	AIControls__ctor_m38E3F626D212C15FA3079208CCB80706A65A1040,
	CreateAndDestroy_Start_m1195C377BC92485AF426AF905B9D7DA1477C9704,
	CreateAndDestroy_Update_m402FA29B26077C1F353EAA2B0BC714FFD91BFAAD,
	CreateAndDestroy__ctor_m7C5F81C9215D9CBAE55839BCC007A06F172DDEC4,
	CreateEnvironment_Start_m131A7D291F7F51FD2A8DB3606ADF9C498F7E2F2B,
	CreateEnvironment__ctor_mCA994A52507CB95072D88B85AFB27F095166DB70,
	Follow_Update_m92DDE81C25F59A864235D4E114204C3818D256EC,
	Follow__ctor_m2F6933C1E6E175D98CD967B20317B7EEF22ED34F,
	GameController_Awake_mE678A4EEF012294E485A814D37D93231A90B4BEC,
	GameController_Start_mCC3F0292799528323E2217A12DB08D98CDD492AE,
	GameController_Update_mA76A2CE1F2AC1AACCDBF913CA6E1EA73DC621CD0,
	GameController_Restart_m4AEFD85E727195AC4517C6FBE488E42A584527B4,
	GameController_ObstacleDestroyed_m7EE7A0F707069AA91AA878A33ABC29755A9C2C80,
	GameController_EnemyDestroyed_mCE0CFA77C3C7496336FE567A3C31AE243A1A108A,
	GameController_SetHealth_m20DF11269095BB75C8490556524FBDDF430592AC,
	GameController__ctor_m9D952052C0A7234373FA5531292FCA8855BE2643,
	Health_Start_m67B788A33501A2CF22C3231FD3FCCD6D1D63A77F,
	Health_ReduceHealth_m51EB454D0BB7FE64EF560988B4CA4D7F43627E3C,
	Health_DamageFlash_mBDB7434500C1E33F30DBF2E88AF14613C4BC46A9,
	Health__ctor_mD50C73D87211EAE260B38B2936F01722E31B9416,
	U3CDamageFlashU3Ed__11__ctor_m4C986E6DF43C7E1FAE7BDD2D7508782D3A55E581,
	U3CDamageFlashU3Ed__11_System_IDisposable_Dispose_m100A2EB7DE727C8034A8A193C67BE6CECCC82FD4,
	U3CDamageFlashU3Ed__11_MoveNext_mD3964081C99FD6E50572757B0F63C086B5EDE6CE,
	U3CDamageFlashU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA22FD54A285B343C4CD828D0394A4B8AB1F00707,
	U3CDamageFlashU3Ed__11_System_Collections_IEnumerator_Reset_mA54E160D0E0D3173D6FB9F0DA417A70F9D3D1762,
	U3CDamageFlashU3Ed__11_System_Collections_IEnumerator_get_Current_m31DCB29C60C0159D6209268A26715276E8380A52,
	MainMenu_StartGame_m8EFE49FE95784A266A683D0CCAAE7C2274981049,
	MainMenu_QuitGame_m9F32E266C6F6CE345067D062258362159D267030,
	MainMenu__ctor_m4D77CEC8F91682A2D9492AE815F89B178BF9717D,
	PlayerControls_Start_mB9489C35C70BB20AA18FC5359CFD9479116004D4,
	PlayerControls_Update_mA38B48E800B339AE629F0CA32238CD57E7271E84,
	PlayerControls_FixedUpdate_mCDB2EC1A56136E6F64211730920AAF2648B575B8,
	PlayerControls__ctor_m892AB10F99012BE66F2524671783FDC93276FB8C,
	Projectile_Start_m065C53350564E17D5A0A0322FF064F8C9697DAB6,
	Projectile_Update_m382C5B499BD4599FE34A04DA3DA0701077C710B2,
	Projectile_Explode_mE3CE07A76C08E543158AC5340235061A2952A37B,
	Projectile_OnTriggerEnter_m4692C1F9E4F8BF76298E88BE8657979932FFA02A,
	Projectile__ctor_m22DAC83BA9B394316027755FD2ADFCA806EE39BB,
	Spawner_SpawnPlayer_mFB56841E48FE1184F00C65D283559AE3F12B8917,
	Spawner_SpawnEnemy_mF1C790E6915E80A69115EABA6208DCB6EBE21CA3,
	Spawner_SpawnEnemyRed_mDD9DD117EC7955F619DC4039A4C542115C29DC14,
	Spawner_Spawn_mF3E97E2A9E5BAAE0B1F9032EB8116B48182B500A,
	Spawner__ctor_m08E8D40AAA40F4329D8A95EEE2B2B6BE842CEB9C,
	UIController_Update_m01A435E125093AE9631843DBDD904FEF5C82D282,
	UIController_TogglePause_m28356A3A207477C5BF1DDCE081B83FE8863CA865,
	UIController_ShowRespawn_m5C2B2E5BEAC57642454E45278900E2EBB327DD70,
	UIController_ShowEndScreen_m4B94B8E8213F807A8A67EC40ED9C9AFFCBA62BBF,
	UIController_QuitGame_m14442F0E80F0D3E4BBF0861C77CAFB32B099AB4A,
	UIController_SetScore_m4256DEB3D595BD099A9A3BBEF63C8BC510DFA692,
	UIController_SetHealth_m499D2973518BC092533210E9F8C6EF580A8CAF48,
	UIController_SetLives_m66A1F47834401B3A2C94FFE668DE317696E04DDA,
	UIController__ctor_m57DAF9FADDD58C79489BCF6474E55D3514E9BA21,
};
static const int32_t s_InvokerIndices[60] = 
{
	1349,
	1349,
	1134,
	1134,
	1134,
	1113,
	1113,
	1113,
	1349,
	1349,
	1349,
	1349,
	1349,
	1349,
	1349,
	1349,
	1349,
	1349,
	1349,
	1349,
	1349,
	1349,
	597,
	1349,
	1349,
	1104,
	1305,
	1349,
	1104,
	1349,
	1327,
	1305,
	1349,
	1305,
	1349,
	1349,
	1349,
	1349,
	1349,
	1349,
	1349,
	1349,
	1349,
	1349,
	1113,
	1349,
	1305,
	1305,
	1305,
	853,
	1349,
	1349,
	1349,
	1131,
	1104,
	1349,
	1134,
	597,
	597,
	1349,
};
extern const CustomAttributesCacheGenerator g_AssemblyU2DCSharp_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule = 
{
	"Assembly-CSharp.dll",
	60,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	g_AssemblyU2DCSharp_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
