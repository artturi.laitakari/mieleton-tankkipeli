using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControls : MonoBehaviour
{
    private Rigidbody rb;
    private Camera mainCamera;
    private float maxRayDistance = 100f;
    private int floorMask;
    private float t;

    public Transform turret;
    public float speed = 5;
    public float turningSpeed = 5;
    public float turretTurningeedSpeed;
    public GameObject projectile;
    public Transform muzzle;
    public float shootingCooldown;
    public AudioSource audiosource;


    void Start()
    {
        t = shootingCooldown;
        rb = GetComponent<Rigidbody>();
        mainCamera = Camera.main;
        floorMask = LayerMask.GetMask("Lattia");
    }
    private void Update()
    {
        if( t <= 0 )
        {
            if (Input.GetButtonDown("Fire1"))
            {
                GameObject proj = Instantiate(projectile, muzzle.position, muzzle.rotation);
                proj.GetComponent<Projectile>().shooterTag = tag;
                t = shootingCooldown;
                audiosource.Play();
            }
        }
        else {
            t -= Time.deltaTime;
        }
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        float inputHorizontal = Input.GetAxis("Horizontal");
        float inputVertical = Input.GetAxis("Vertical");

        if (inputHorizontal != 0)
        {
            Vector3 turning = Vector3.up * inputHorizontal * turningSpeed;
            rb.angularVelocity = turning;
        }

        if (inputVertical != 0)
        {
            Vector3 movement = transform.forward * inputVertical * speed;
            rb.velocity = movement;
        }
        Ray ray = mainCamera.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit, maxRayDistance, floorMask))
        {
            Vector3 targetDirection = hit.point - turret.position;
            targetDirection.y = 0f;
            Vector3 turningDirection = Vector3.RotateTowards(turret.forward, targetDirection, turretTurningeedSpeed * Time.deltaTime, 0f);
            turret.rotation = Quaternion.LookRotation(turningDirection);
        }
    }
}
