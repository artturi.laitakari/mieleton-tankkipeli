using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreateAndDestroy : MonoBehaviour
{
    private ParticleSystem ps;
    private AudioSource audiosource;

    // Start is called before the first frame update
    void Start()
    {
        ps = GetComponent<ParticleSystem>();
        ps.Play();
        audiosource = GetComponent<AudioSource>();
        audiosource.Play();
    }

    // Update is called once per frame
    void Update()
    {
        if(!ps.isPlaying && !audiosource.isPlaying)
        {
            Destroy(gameObject);
            
        }
        
    }
}
