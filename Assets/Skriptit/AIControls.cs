using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIControls : MonoBehaviour
{
    public Transform turret;
    public float speed = 5;
    public float turningSpeed = 5;
    public float turretTurningeedSpeed;
    public GameObject projectile;
    public Transform muzzle;
    public float shootingCooldown;
    public float detectRange;
    public float stoppingRange;
    public float switchTargetDistance;
    public float switchDistance;

    public float AIDelay;

    private Rigidbody rb;
    private float t;
    private float AIt;
    private GameObject targetObject;
    private Vector3 target;
    private enum State { forward, left, right, back, stop };
    private State state;
    private State nextState;

    private int obstacleMask;
    public AudioSource audiosource;


    void Start()
    {
        rb = GetComponent<Rigidbody>();
        t = 0f;
        AIt = 0f;
        obstacleMask = LayerMask.GetMask("Este");
        state = State.forward;
        nextState = State.forward;
    }

    void FixedUpdate()
    {
        Vector3 currentRotation = rb.rotation.eulerAngles;
        rb.rotation = Quaternion.Euler(0f, currentRotation.y, 0f);
        t -= Time.deltaTime;

        if( Vector3.Distance(transform.position, target) < switchTargetDistance)
        {
            float randomx = Random.Range(-switchDistance, switchDistance);
            float randomz = Random.Range(-switchDistance, switchDistance);

            target += new Vector3(randomx, 0f, randomz);
        }
        // etsi pelaaja
        if ( targetObject != null) 
        {
            if(Vector3.Distance(transform.position, targetObject.transform.position) < detectRange)
            {
                if(!Physics.Linecast(transform.position, targetObject.transform.position, obstacleMask) )
                {
                    target = targetObject.transform.position;
                    if(t < 0)
                    {
                        GameObject proj = Instantiate(projectile, muzzle.position, muzzle.rotation);
                        proj.GetComponent<Projectile>().shooterTag = tag;
                        t = shootingCooldown;
                        audiosource.Play();
                    }

                    if (Vector3.Distance(target, transform.position) < stoppingRange)
                    {
                        nextState = State.stop;
                    }
                }
            }
        }
        else
        {
            targetObject = GameObject.FindGameObjectWithTag("Player");
        }

        // liiku pelaajaa kohti
        float angle = Vector3.SignedAngle(transform.forward, target - transform.position, Vector3.up);

        if (AIt < 0f)
        {
            state = nextState;
            AIt = AIDelay;
        }
        else
        {
            AIt -= Time.deltaTime;
        }
        switch (state) // tilakone
        {
            case State.forward:
                Forward(angle);
                break;
            case State.left:
                Turning(-1f);
                Move(1f);
                break;
            case State.right:
                Turning(1f);
                Move(1f);
                break;
            case State.back:
                Move(-1f);
                nextState = State.forward;
                break;
            default: // stop
                Move(0f);
                Turning(0f);
                nextState = State.forward;
                break;
        }
        Vector3 targetDirection = target - turret.position;
        targetDirection.y = 0f;
        Vector3 turningDirection = Vector3.RotateTowards(turret.forward, targetDirection, turretTurningeedSpeed * Time.deltaTime, 0f);
        turret.rotation = Quaternion.LookRotation(turningDirection);
    }
    private void Forward(float angle)
    {
        if (angle < 0)
        {
            Turning(-1f);
        }
        else if (angle > 0)
        {
            Turning(1f);
        }
        if (Mathf.Abs(angle) < 90)
        {
            Move(1f);
        }
    }

    private void Move(float input)
    {
        Vector3 movement = transform.forward * input * speed;
        rb.velocity = movement;
    }
    private void Turning(float input)
    {
        Vector3 turning = Vector3.up * input * turningSpeed;
        rb.angularVelocity = turning;
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag != "Este" && other.tag != "Sein�")
        {
            return;
        }
        RaycastHit leftHit;
        RaycastHit rightHit;

        float leftLength = 0f;
        float rightLength = 0f;

        if(Physics.Raycast(transform.position, transform.forward + transform.right * -1, out leftHit, Mathf.Infinity, obstacleMask))
        {
            leftLength = leftHit.distance;
        }
        if (Physics.Raycast(transform.position, transform.forward + transform.right, out rightHit, Mathf.Infinity, obstacleMask))
        {
            rightLength = rightHit.distance;
        }
        if(leftLength > rightLength)
        {
            target = leftHit.point;
            state = State.left;
        } else
        {
            target = rightHit.point;
            state = State.right;
        }
    }
    private void OnTriggerExit(Collider other)
    {
        nextState = State.forward;
    }
    private void OnTCollisionEnter(Collision other)
    {
        
        if (!other.gameObject.CompareTag("Este") && !other.gameObject.CompareTag("Sein�"))
        {
            return;
        }
        nextState = State.back;

    }
}
