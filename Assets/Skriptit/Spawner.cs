using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    public GameObject player;
    public GameObject enemy;
    public GameObject enemyRed;
    public Collider area;
    public float range;

    public GameObject SpawnPlayer()
    {
        return Spawn(player);
    }
    public GameObject SpawnEnemy()
    {
        return Spawn(enemy);
    }
    public GameObject SpawnEnemyRed()
    {
        return Spawn(enemyRed);
    }
    private GameObject Spawn(GameObject obj)
    {
        Vector3 minpoint = area.bounds.min;
        Vector3 maxpoint = area.bounds.max;

        float randomX = Random.Range(minpoint.x, maxpoint.x);
        float randomZ = Random.Range(minpoint.z, maxpoint.z);

        Vector3 position = new Vector3(randomX, 0.46f, randomZ);

        Collider[] colliders = Physics.OverlapSphere(position, range);
        for(int i = 0; i < colliders.Length; i++)
        {
            if(colliders[i].CompareTag("Este"))
            {
                Destroy(colliders[i].gameObject);
            }
        }
        GameObject newObj = Instantiate(obj, position, new Quaternion());
        return newObj;
    }
}
