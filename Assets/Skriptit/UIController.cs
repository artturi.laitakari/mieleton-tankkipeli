using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIController : MonoBehaviour
{
    public Text scoreText;
    public Text healthText;
    public Text livesText;

    public GameObject pausemenu;
    public GameObject respawn;
    public GameObject endGame;
    public Text endScore;


    void Update()
    {
        if(Input.GetButtonDown("Cancel"))
        {
            TogglePause();
        }
    }
    public void TogglePause()
    {
        if(pausemenu.activeInHierarchy)
        {
            pausemenu.SetActive(false);
            Time.timeScale = 1f;
        } else if(!endGame.activeInHierarchy && !respawn.activeInHierarchy)
        {
            pausemenu.SetActive(true);
            Time.timeScale = 0f;
        }
    }
    public void ShowRespawn(bool show)
    {
        respawn.SetActive(show);
    }
    public void ShowEndScreen(int score)
    {
        endGame.SetActive(true);
        endScore.text = "Pisteesi: " + score;
    }

    public void QuitGame()
    {
#if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
#else
        Application.Quit();
#endif
    }
    public void SetScore(float score)
    {
        scoreText.text = "Pisteet: " + score;
    }
    public void SetHealth(int current, int max)
    {
        healthText.text = "Kesto: " + current + " / " + max;
    }
    public void SetLives(int current, int max)
    {
        livesText.text = "El�m�t: " + current + " / " + max;
    }
}

