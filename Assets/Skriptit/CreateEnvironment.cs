using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreateEnvironment : MonoBehaviour
{
    public int boxes;
    public int spheres;
    public GameObject box;
    public GameObject sphere;
    public Collider ground;
    public int minSize;
    public int maxSize;

    void Start()
    {
        Vector3 minPoint = ground.bounds.min;
        Vector3 maxPoint = ground.bounds.max;

        for (int i=0; i < boxes; i++)
        {
            float randomX = Random.Range(minPoint.x, maxPoint.x);
            float randomZ = Random.Range(minPoint.z, maxPoint.z);
            float randomSize = Random.Range(minSize, maxSize);
            Vector3 position = new Vector3(randomX, maxPoint.y + randomSize / 2, randomZ);
            GameObject spawned_box = Instantiate(box, position, new Quaternion());
            spawned_box.transform.localScale *= randomSize;
            spawned_box.GetComponent<Health>().maxHealth = (int) randomSize + 1;
        }
        for (int i = 0; i < spheres; i++)
        {
            float randomX = Random.Range(minPoint.x, maxPoint.x);
            float randomZ = Random.Range(minPoint.z, maxPoint.z);
            float randomY = Random.Range(minSize, maxSize);
            Vector3 position = new Vector3(randomX, maxPoint.y + randomY, randomZ);
            Instantiate(sphere, position, new Quaternion(), transform);
        }
    }
}
