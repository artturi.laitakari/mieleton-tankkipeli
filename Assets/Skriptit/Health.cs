using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Health : MonoBehaviour
{
    public int maxHealth=3;
    public GameObject explosion;
    public float damageFlashTime = 1f;

    private int currentHealth;
    private Color originalColor;
    public Color damageColor = Color.red;
    private float t;
    private MeshRenderer[] meshRenderers;
    private AudioSource audiosource;

    void Start()
    {
        currentHealth = maxHealth;
        meshRenderers = GetComponentsInChildren<MeshRenderer>();
        originalColor = meshRenderers[0].material.color;
        audiosource = GetComponent<AudioSource>();
        if (gameObject.tag == "Player")
        {
            GameController.instance.SetHealth(currentHealth, maxHealth);
        }
    }
    public void ReduceHealth(int damage)
    {
        StartCoroutine(DamageFlash());
        currentHealth -= damage;
        audiosource.Play();
        if(gameObject.tag == "Player")
        {
            GameController.instance.SetHealth(currentHealth, maxHealth);
        }
        if(currentHealth <= 0 )
        {
            Instantiate(explosion, transform.position, new Quaternion());
            if(gameObject.CompareTag("Enemy"))
            {
                GameController.instance.EnemyDestroyed();
            }
            if (gameObject.CompareTag("Este"))
            {
                GameController.instance.ObstacleDestroyed();
            }
            Destroy(gameObject);
        }
    }
    private IEnumerator DamageFlash()
    {
        t = damageFlashTime;
        while ( t > 0)
        {
            t -= Time.deltaTime;
            Color newColor = Color.Lerp(originalColor, damageColor, t / damageFlashTime);            

            foreach (MeshRenderer r in meshRenderers)
            {
                r.material.color = newColor;
            }
            yield return null;
        }
        foreach (MeshRenderer r in meshRenderers)
        {
            r.material.color = originalColor;
        }
    }
}
