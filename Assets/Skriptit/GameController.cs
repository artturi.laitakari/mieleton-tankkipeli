using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour
{
    public Spawner spawner;
    public float score;
    public float scorePerTank;
    public float scorePerObstacle = 1f;
    public int lives;
    public int enemyStartingAmount;
    public int maxEnemies;

    private int currentLives;
    private int currentEnemyAmount;
    private GameObject player;
    public static GameController instance;
    public UIController ui;
    public AudioSource music;
    public AudioSource respawnsound;
    public AudioSource neardeath;
    public void Awake()
    {
        instance = this;
    }

    void Start()
    {
        for(int i = 0; i < enemyStartingAmount; i++)
        {
            spawner.SpawnEnemy();
        }
        player = spawner.SpawnPlayer();
        score = 0f;
        currentLives = lives;
        currentEnemyAmount = enemyStartingAmount;
        ui.SetScore(score);
        ui.SetLives(currentLives, lives);
        ui.SetHealth(currentLives, lives);
    }

    void Update()
    {
        if (player == null)
        {
            if (currentLives > 0)
            {
                ui.ShowRespawn(true);
                if (Input.GetButtonDown("Restart"))
                {
                    player = spawner.SpawnPlayer();
                    currentLives--;
                    ui.SetLives(currentLives, lives);
                    ui.ShowRespawn(false);
                    respawnsound.Play();
                    if (currentLives == 1)
                    {
                        neardeath.Play();
                        music.Stop();
                    }
                }
            }
            else
            {
                ui.ShowEndScreen((int)score);
            }
        } 
    }

    public void Restart()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
    public void ObstacleDestroyed()
    {
        score += scorePerObstacle;
        ui.SetScore(score);
    }

    public void EnemyDestroyed()
    {
        score += scorePerTank;
        ui.SetScore(score);

        spawner.SpawnEnemy();
        if (currentEnemyAmount < maxEnemies)
        {
            spawner.SpawnEnemyRed();
            currentEnemyAmount++;
        }
    }
    public void SetHealth(int current, int MaxHealth)
    {
        if(current < 0)
        {
            current = 0;
        }
        ui.SetHealth(current, MaxHealth);
    }

}
